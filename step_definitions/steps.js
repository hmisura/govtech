const { client } = require('nightwatch-cucumber');
const { Given, Then, When } = require('cucumber');
let username = "";
let password = "";
const url = "bgp-qa.gds-gov.tech";

Given(/^user navigtes to the BGP test environment$/, () => {
  return client
    .maximizeWindow();
});

Given(/^user enter "(.*?)" in the username field$/, (un) => {
  return username = un
});

Given(/^user enter "(.*?)" in the password field$/, (pw) => {
  return password = pw
});

Given(/^user click on the sign-in button$/, () => {
  return client
    .url('https://' + username + ':' + password + '@' + url)
});

Given(/^user click on the log-in button$/, () => {
  return client
    .click('a[id=login-button]')
});

Given(/^user select predefined user "(.*?)"$/, (predefined_user) => {
  return client
    .click('option[value='+predefined_user+']')
});

Given(/^user click on the login button$/, () => {
  return client
    .click('button[type=submit]')
});

Given(/^user click on Get new grant$/, () => {
  return client
    .useXpath()
    .waitForElementVisible('//*[@id="grants"]/div[2]/div/a[2]/section/div', 20000)
    .click('//*[@id="grants"]/div[2]/div/a[2]/section/div')
});

Given(/^user select business as Building & Constructions$/, () => {
  return client
    .waitForElementVisible('//input[@id="Building & Construction"]', 5000)
    .click('//input[@id="Building & Construction"]')
});

Given(/^user select sub-sector as Builders$/, () => {
  return client
    .click('//input[@id="Builders (Contractors)"]')
});

Given(/^user need this grant to upgrade key business areas$/, () => {
  return client
    .click('//*[@id="grant-wizard"]/div[1]/div/div/div[2]/div/label')
});

Given(/^user describe the area as pre-scoped productivity solutions$/, () => {
  return client
    .click('//*[@id="grant-wizard"]/div[1]/div/div/div[1]/div/label')
    .waitForElementVisible('//*[@id="go-to-grant"]',1000)
    .click('//*[@id="go-to-grant"]')
});

Given(/^user proceed to application form$/, () => {
  return client
    .waitForElementVisible('//*[@id="js-app"]/div/div[2]/div/div/div/div[2]/section/section/div[3]/button',5000)
    .execute('scrollTo(0, 1000)')
    .click('//*[@id="js-app"]/div/div[2]/div/div/div/div[2]/section/section/div[3]/button')
    .pause(3000)
});

Then(/^answer is not selected by the user$/, () => {
  return  client
    .waitForElementVisible('//*[@id="react-eligibility-user_agreement_check-false"]',5000)
    .execute('scrollTo(0, 1000)')
    .expect.element('//*[@id="react-eligibility-user_agreement_check-false"]').to.not.be.selected
});

Then(/^user verify that the Next button is disabled$/, () => {
  return client
  .getAttribute('//*[@id="next-btn"]','disabled',function(result){
    this.assert.equal(result.value, "true");
  });
});

Then(/^user verify that Eligibility side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[1]/a/span').to.be.present
});

Then(/^user verify that Contact Details side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[2]/a/span').to.not.be.visible
});

Then(/^user verify that Proposal side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[3]/a/span').to.not.be.present
});

Then(/^user verify that Cost side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[4]/a/span').to.not.be.present
});

Then(/^user verify that Business Impact side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[5]/a/span').to.not.be.present
});

Then(/^user verify that Declare & Review side menu option is disabled$/, () => {
  return client
  .expect.element('//*[@id="js-app"]/div/div/div[2]/div[1]/div/div/ul/li[6]/a/span').to.not.be.present
});
When(/^user select radio button Yes$/, () => {
  return client 
      .click('//*[@id="react-eligibility-user_agreement_check-true"]')
      .pause(10000)
});

