Feature: Business Grant Portal Test

Background:
  Given user navigtes to the BGP test environment
  And user enter "public" in the username field
  And user enter "Let$BeC001" in the password field
  And user click on the sign-in button
  And user click on the log-in button
  And user select predefined user "S9111111A"
  And user click on the login button
  And user click on Get new grant
  And user select business as Building & Constructions
  And user select sub-sector as Builders
  And user need this grant to upgrade key business areas
  And user describe the area as pre-scoped productivity solutions
  And user proceed to application form

Scenario: AC 1-1 (When no answer is selected for the question ‘Does the applicant meet the eligibility criteria’)

  When answer is not selected by the user
  Then user verify that the Next button is disabled
  And user verify that Eligibility side menu option is disabled
  And user verify that Contact Details side menu option is disabled
  And user verify that Proposal side menu option is disabled
  And user verify that Cost side menu option is disabled
  And user verify that Business Impact side menu option is disabled
  And user verify that Declare & Review side menu option is disabled
  # When user select radio button Yes
  